from django.shortcuts import render

# Create your views here.
def getBooksPage(request):
    return render(request, "googleBooks/googleBooksPage.html")
