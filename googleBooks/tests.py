from django.test import TestCase
from django.test import TestCase,Client
from django.urls import resolve
from . import views
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import unittest, time
from selenium.webdriver.common.keys import Keys
from datetime import date, time
from time import sleep
from django.test import LiveServerTestCase

# Create your tests here.
class TestGoogleBooks(TestCase):

    def test_page_success(self):
        response = Client().get("/")
        self.assertEqual(response.status_code,200)

    def test_page_html_correct(self):
        response = Client().get("/")
        self.assertTemplateUsed(response,"googleBooks/googleBooksPage.html")

    def test_page_views_func_correct(self):
        response = resolve("/")
        self.assertEqual(response.func, views.getBooksPage)

class TestFunctionalGoogleBooks(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(executable_path="./chromedriver", chrome_options=chrome_options)
        super(TestFunctionalGoogleBooks,self).setUp()
        
    def tearDown(self):
        self.browser.quit()
        super(TestFunctionalGoogleBooks,self).tearDown()

    def test_functionality(self):
        driver = self.browser
        driver.get(self.live_server_url)

        print(driver)
        search_bar = driver.find_element_by_id("search_bar")
        search_bar.send_keys('naruto')
        search_btn = driver.find_element_by_id("search_btn")
        search_btn.click()
        sleep(3)
        self.assertIn("Naruto",driver.page_source)
        sleep(1)
